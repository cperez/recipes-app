# recipes-app

Built using Next.js and deployed on vercel. No third party libraries used except for `axios` which was a requirement.

I didn't use a CSS preprocessor and used CSS modules instead of naming the classes using BEM conventions or similar styles.

## Pending items

- Pagination
- Show favorites in the home page

### Pagination

The API we had to use is pretty limited on the free tier and you have to get one random recipe at a time so that makes pagination more cumbersome to implement.

If I had more time what I would do is create an API endpoint in Next.js side which would do the work behind the scenes to pull all the recipes, e.g. using the `List all meals by first letter` endpoint in the background, could be when the server starts, to have them in memory so pagination would be implemented and always return a consistent result set which is critical for this.

I would add query parameters, so you can bookmark and share a specific page if needed.

On a bigger project definitely storing the data in memory doesn't scale so something like Redis could be used instead since we don't own the third party API.

### Showing the favorites

This is something that can be implemented with the code that is in the `extra` branch but instead of just storing an array of recipe IDs it would store an object where the `key` is the ID and the `value` is the name.

We would need a different UI to make that visible and one option is adding a "toolbar" to show that button and either move the search button there or keep it in the existing place.

The list of favorites would be another route in Next.js which doesn't require SSR since we store the data on the client.

## Completed after the four hours limit

- Add to favorites
- Share on twitter

## Other improvements that can be made

### Search

In the search modal I didn't have enough time to lock the focus to the content inside the modal when you tab with your keyboard so the focus can be moved to elements that are not visible.

This can be implemented by adding a `focus` event listener that checks whether the element that will get focus is inside or outside the modal and if it is outside then focus an element instead which could be the modal itself or the last `focusable` element depending on whether the user is doing `Tab` or `Shift+Tab`.

In the same component it would also be better to keep a reference to the `Search` button to move the focus back to it once the modal is closed in case the user canceled the search.

### State management

I didn't see the need to make things complex so only React state is used but if we wanted to reduce the number of network calls we have a few options:

- Use a service worker to store the network responses and images in a cache.
- Use something like `react-query`or `swr` which provide a client side cache out of the box.
- If we were the owners of the API another option would be to use GraphQL to avoid fetching data that we don't need and if Apollo is used then its cache can also be leveraged.
