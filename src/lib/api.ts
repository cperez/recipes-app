import axios from "axios";
import { Recipe } from "../types";

// convert the API response to our own type
const mapMeal = (meal: any): Recipe => {
  return {
    area: meal.strArea,
    category: meal.strCategory,
    id: meal.idMeal,
    imageUrl: meal.strMealThumb,
    // we need to show the ingredient with its measure
    ingredients: Object.keys(meal)
      .filter((key) => key.startsWith("strIngredient") && meal[key])
      .map((key, index) => ({
        description: meal[key],
        measure: meal[`strMeasure${index + 1}`],
      })),
    // we want lines to build paragraphs
    instructions: meal.strInstructions
      .split("\r\n")
      .map((line: string) => line.trim())
      .filter((line: string) => !!line),
    name: meal.strMeal,
    tags: meal.strTags?.split(",") || [],
  };
};

export const fetchRandomRecipe = (): Promise<Recipe> => {
  return axios
    .get("https://www.themealdb.com/api/json/v1/1/random.php")
    .then((response) => {
      if (response.data.meals) {
        const [meal] = response.data.meals.map(mapMeal);

        return meal;
      }
      return null;
    });
};

export const fetchRecipe = (id: string): Promise<Recipe> => {
  return axios
    .get("https://www.themealdb.com/api/json/v1/1/lookup.php", {
      params: {
        i: id,
      },
    })
    .then((response) => {
      if (response.data.meals) {
        const [meal] = response.data.meals.map(mapMeal);

        return meal;
      }

      return null;
    });
};

export const searchRecipes = (name: string): Promise<Recipe[]> => {
  return axios
    .get("https://www.themealdb.com/api/json/v1/1/search.php", {
      params: {
        s: name,
      },
    })
    .then((response) => {
      if (response.data.meals) {
        return response.data.meals.map(mapMeal);
      }

      return [];
    });
};
