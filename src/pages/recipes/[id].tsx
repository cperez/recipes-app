import React from "react";
import { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import BackIcon from "../../components/common/icons/BackIcon";
import NavigationBar from "../../components/common/navigation/NavigationBar";
import NavigationBarButton from "../../components/common/navigation/NavigationBarButton";
import RecipeDetail from "../../components/recipes/RecipeDetail";
import { fetchRecipe } from "../../lib/api";
import { Recipe } from "../../types";

interface RecipeDetailPageProps {
  recipe: Recipe;
}

const RecipeDetailPage: NextPage<RecipeDetailPageProps> = ({ recipe }) => {
  return (
    <>
      <Head>
        <title>{recipe.name} - Recipes app</title>
      </Head>

      <header className="sticky">
        <NavigationBar>
          <Link href="/" passHref>
            <NavigationBarButton component="a" title="Go back to recipes list">
              <BackIcon />
            </NavigationBarButton>
          </Link>
        </NavigationBar>
      </header>

      <main>
        <RecipeDetail recipe={recipe} />
      </main>
    </>
  );
};

export const getServerSideProps: GetServerSideProps<
  RecipeDetailPageProps,
  { id: string }
> = async (context) => {
  let recipe;

  try {
    const id = context.params?.id;

    if (id) {
      recipe = await fetchRecipe(id);
    }
  } catch (e) {}

  if (!recipe) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      recipe,
    },
  };
};

export default RecipeDetailPage;
