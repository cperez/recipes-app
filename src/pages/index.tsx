import type { GetServerSideProps, NextPage } from "next";
import Head from "next/head";
import Home from "../components/home/Home";
import { fetchRandomRecipe } from "../lib/api";
import { Recipe } from "../types";

interface HomePageProps {
  recipes: Recipe[];
}

const HomePage: NextPage<HomePageProps> = ({ recipes }) => {
  return (
    <div>
      <Head>
        <title>Recipes app</title>
        <link rel="icon" href="/favicon.svg" />
      </Head>

      <Home recipes={recipes} />
    </div>
  );
};

const PAGE_SIZE = 5;

export const getServerSideProps: GetServerSideProps = async () => {
  let recipes;

  try {
    const promises = [];

    for (let i = 0; i < PAGE_SIZE; i++) {
      promises.push(fetchRandomRecipe());
    }

    recipes = await Promise.all(promises);
  } catch (e) {
    // we would put some error handling code here to either retry or show an error
  }

  if (!recipes) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      recipes,
    },
  };
};

export default HomePage;
