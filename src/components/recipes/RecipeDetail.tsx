import Image from "next/image";
import { Recipe } from "../../types";
import styles from "./RecipeDetail.module.css";

interface RecipeDetailProps {
  recipe: Recipe;
}

const RecipeDetail = ({ recipe }: RecipeDetailProps) => {
  return (
    <>
      <div className={styles.detailImageContainer}>
        <Image
          alt=""
          src={recipe.imageUrl}
          layout="fill"
          objectFit="cover"
          objectPosition="center"
          priority
        />
      </div>

      <section className={styles.detailContainer}>
        <h1>{recipe.name}</h1>

        <ul title="Ingredients">
          {recipe.ingredients.map((ingredient, index) => (
            <li key={index} className={styles.detailIngredient}>
              {ingredient.measure} {ingredient.description}
            </li>
          ))}
        </ul>

        <h2>Directions</h2>

        {recipe.instructions.map((instruction, index) => (
          <p key={index} className={styles.detailInstructionsStep}>
            {instruction}
          </p>
        ))}
      </section>
    </>
  );
};

export default RecipeDetail;
