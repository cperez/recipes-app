import Link from "next/link";
import Image from "next/image";
import { Recipe } from "../../types";
import styles from "./RecipeListItem.module.css";

interface RecipeListItemProps {
  recipe: Recipe;
}

const RecipeListItem = ({ recipe }: RecipeListItemProps) => {
  return (
    <li className={styles.item}>
      <Link href={`/recipes/${recipe.id}`}>
        <a className={styles.itemLink}>
          <div className={styles.itemName}>{recipe.name}</div>

          <div className={styles.itemImageContainer}>
            <Image
              alt=""
              src={recipe.imageUrl}
              layout="fill"
              objectFit="cover"
              objectPosition="center"
            />
          </div>
        </a>
      </Link>
    </li>
  );
};

export default RecipeListItem;
