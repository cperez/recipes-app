import React from "react";
import { Recipe } from "../../types";
import RecipeListItem from "./RecipeListItem";
import styles from "./RecipeList.module.css";

interface RecipeListProps {
  recipes: Recipe[];
}

const RecipeList = ({ recipes }: RecipeListProps) => {
  return (
    <ul className={styles.recipeList}>
      {recipes.map((recipe) => (
        <RecipeListItem key={recipe.id} recipe={recipe} />
      ))}
    </ul>
  );
};

export default RecipeList;
