import { useState } from "react";
import Image from "next/image";
import dynamic from "next/dynamic";
import FloatingButton from "../common/button/FloatingButton";
import RecipeListItem from "../recipes/RecipeListItem";
import { Recipe } from "../../types";
import styles from "./Home.module.css";
import RecipeList from "../recipes/RecipeList";

const Search = dynamic(() => import("../search/Search"));

interface HomeProps {
  recipes: Recipe[];
}

const Home = ({ recipes }: HomeProps) => {
  const [showSearch, setShowSearch] = useState(false);

  return (
    <>
      {showSearch && <Search onCancel={() => setShowSearch(false)} />}

      <header className={styles.homeHeader}>
        <div className={styles.homeHeaderImagesContainer}>
          <Image
            aria-hidden="true"
            alt=""
            src={require("./home-background.jpg")}
            layout="fill"
            objectFit="cover"
            objectPosition="bottom"
            placeholder="blur"
          />

          <div className={styles.homeLogoContainer}>
            <Image
              aria-hidden="true"
              alt=""
              className={styles.homeLogo}
              src={require("./home-logo.png")}
              width={121}
              height={131}
            />
          </div>
        </div>

        <h1 className={styles.homeAppName}>Recipes of the day</h1>
      </header>

      <main>
        <FloatingButton
          icon="search"
          aria-label="Show search form"
          onClick={() => setShowSearch(true)}
        />

        <div className={styles.homeRecipesList}>
          <RecipeList recipes={recipes} />
        </div>
      </main>
    </>
  );
};

export default Home;
