import React, { useEffect, useState } from "react";
import { createPortal } from "react-dom";
import BackIcon from "../common/icons/BackIcon";
import NavigationBar from "../common/navigation/NavigationBar";
import NavigationBarButton from "components/common/navigation/NavigationBarButton";
import { searchRecipes } from "../../lib/api";
import { Recipe } from "../../types";
import SearchForm from "./SearchForm";
import styles from "./Search.module.css";
import SearchResults from "./SearchResults";

type SearchStatus = "idle" | "loading" | "error" | "done" | "invalid";

interface SearchProps {
  onCancel: () => void;
}

const Search = ({ onCancel }: SearchProps) => {
  const [recipes, setRecipes] = useState<Recipe[]>([]);
  const [status, setStatus] = useState<SearchStatus>("idle");

  const handleSearch = (term: string) => {
    if (term.length === 0) {
      setStatus("idle");
      return;
    }

    if (term.length > 1 && term.length < 3) {
      setStatus("invalid");
      return;
    }

    setStatus("loading");

    searchRecipes(term).then(
      (foundRecipes) => {
        setRecipes(foundRecipes);
        setStatus("done");
      },
      () => setStatus("error")
    );
  };

  // we don't want to scroll what is behind this component
  useEffect(() => {
    document.body.style.overflow = "hidden";

    return () => {
      document.body.style.overflow = "auto";
    };
  }, []);

  useEffect(() => {
    const handleKeydown = (e: KeyboardEvent) => {
      if (e.key === "Escape") {
        onCancel();
      }
    };

    document.addEventListener("keydown", handleKeydown);

    return () => {
      document.removeEventListener("keydown", handleKeydown);
    };
  }, [onCancel]);

  return createPortal(
    <div
      role="dialog"
      aria-modal="true"
      className={styles.searchContainer}
      tabIndex={0}
    >
      <div className={styles.searchNavBar}>
        <NavigationBar>
          <NavigationBarButton component="button" onClick={onCancel}>
            <BackIcon />
          </NavigationBarButton>

          <SearchForm onSearch={handleSearch} />
        </NavigationBar>
      </div>

      <div aria-live="polite" className={styles.searchResults}>
        {status === "invalid" && (
          <div className={styles.searchMessage}>
            You need to enter at least three characters.
          </div>
        )}

        {status === "done" && !recipes.length && (
          <div className={styles.searchMessage}>No recipes found</div>
        )}

        {status === "done" && !!recipes.length && (
          <SearchResults recipes={recipes} />
        )}
      </div>
    </div>,
    document.body
  );
};

export default Search;
