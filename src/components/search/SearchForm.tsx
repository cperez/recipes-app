import React, { FormEventHandler, useCallback } from "react";
import styles from "./SearchForm.module.css";

interface SearchFormProps {
  onSearch: (term: string) => void;
}

const SearchForm = ({ onSearch }: SearchFormProps) => {
  const handleSearch = useCallback<FormEventHandler<HTMLFormElement>>(
    (e) => {
      e.preventDefault();

      const data = new FormData(e.currentTarget);

      onSearch((data.get("searchTerm") as string).trim());
    },
    [onSearch]
  );

  return (
    <form
      action=""
      role="search"
      aria-label="Search recipes"
      className={styles.searchForm}
      onSubmit={handleSearch}
      noValidate
    >
      <label htmlFor="searchTerm" hidden>
        Recipe name
      </label>

      <input
        id="searchTerm"
        name="searchTerm"
        type="search"
        autoCorrect="off"
        autoComplete="off"
        autoFocus={true}
        className={styles.searchInput}
        placeholder="I'm craving..."
      />

      <button type="submit" hidden>
        Search
      </button>
    </form>
  );
};

export default SearchForm;
