import React from "react";
import Link from "next/link";
import { Recipe } from "../../types";
import styles from "./SearchResults.module.css";

interface SearchResultsProps {
  recipes: Recipe[];
}

const SearchResults = ({ recipes }: SearchResultsProps) => {
  return (
    <ul title="Search results">
      {recipes.map((recipe) => (
        <li key={recipe.id}>
          <Link href={`/recipes/${recipe.id}`}>
            <a className={styles.searchResultsItemLink}>{recipe.name}</a>
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default SearchResults;
