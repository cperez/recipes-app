import React from "react";
import SearchIcon from "../icons/SearchIcon";
import styles from "./FloatingButton.module.css";

type FloatingButtonProps = Omit<
  React.ComponentPropsWithRef<"button">,
  "className"
> & {
  icon: "search";
};

const iconMap = {
  search: SearchIcon,
};

const FloatingButtonWithRef = React.forwardRef<
  HTMLButtonElement,
  FloatingButtonProps
>(function FloatingButton({ icon, ...props }, ref) {
  const Icon = iconMap[icon];

  return (
    <button type="button" className={styles.button} ref={ref} {...props}>
      <Icon />
    </button>
  );
});

export default FloatingButtonWithRef;
