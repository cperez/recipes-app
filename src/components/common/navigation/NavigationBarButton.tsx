import React from "react";
import styles from "./NavigationBarButton.module.css";

type ButtonProps = { component: "button" } & Omit<
  React.ComponentPropsWithRef<"button">,
  "className"
>;
type LinkProps = { component: "a" } & Omit<
  React.ComponentPropsWithRef<"a">,
  "className"
>;

type NavigationBarButtonProps = ButtonProps | LinkProps;

const NavigationBarButtonWithRef = React.forwardRef<
  HTMLButtonElement | HTMLAnchorElement,
  NavigationBarButtonProps
>(function NavigationBarButton({ component: Component, ...props }, ref) {
  return (
    // @ts-ignore
    <Component className={styles.navigationBarButton} ref={ref} {...props} />
  );
});

export default NavigationBarButtonWithRef;
