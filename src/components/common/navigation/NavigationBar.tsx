import React, { ReactNode } from "react";
import styles from "./NavigationBar.module.css";

interface NavigationBarProps {
  children?: ReactNode;
}

const NavigationBar = ({ children }: NavigationBarProps) => {
  return <nav className={styles.navigationContainer}>{children}</nav>;
};

export default NavigationBar;
