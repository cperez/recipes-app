export interface Recipe {
  area: string;
  category: string;
  id: string;
  name: string;
  imageUrl: string;
  instructions: string[];
  ingredients: RecipeIngredient[];
  tags: string[];
}

export interface RecipeIngredient {
  description: string;
  measure: string;
}
